import { CHANGE_USER, TODO_ITEM_CHANGE, UPDATE_LIST, UPDATE_ITEM, EDIT_ITEM, CANCEL } from "./type";
import { combineReducers } from 'redux';

// user Reducer
const initialUserState = { user: { username: '', password: '' }, isLogined: false };

const userReducer = (state = initialUserState, action) => {
  switch (action.type) {
    case CHANGE_USER:
      const { key, value } = action.payload;

      return {
        ...state,
        user: { ...state.user, [key]: value }
      }
    default: return state
  }
}

// todo Reducer
const initialTodoState = {
  todoList: [
    {name: 'Learn HTML', level: 1, date: '2020-01-01'},
    {name: 'Learn CSS', level: 2, date: '2020-01-01'},
    {name: 'Learn JAVASCRIPT', level: 3, date: '2020-01-01'},
    {name: 'Learn BOOTSTRAP', level: 4, date: '2020-01-01'},
    {name: 'Learn JQUERY', level: 5, date: '2020-01-01'}
  ],
  todoItem: {name: '',level: '1',date: ''},
  isAdd: true,
  indexUpdate: 0
}

const todoReducer = (state = initialTodoState, action) => {
  switch (action.type) {
    case TODO_ITEM_CHANGE: return {
      ...state,
      todoItem: action.payload
    }
    case UPDATE_LIST: return {
      ...state,
      todoList: action.payload
    }
    case UPDATE_ITEM:
      return {
        ...state,
        todoList: action.payload,
        isAdd: true
      }
    case EDIT_ITEM:
      const { isAdd, indexUpdate, todoItem } = action.payload;
      return {
        ...state,
        isAdd,
        indexUpdate,
        todoItem
      }
    case CANCEL: return {
      ...state,
      isAdd: true,
      todoItem: {name: '',level: '1',date: ''}
    }
    default: return state;
  }
}

const rootReducer = combineReducers({
  user: userReducer,
  todo: todoReducer
});

export default rootReducer;