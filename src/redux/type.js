export const CHANGE_USER = 'CHANGE_USER';

export const TODO_ITEM_CHANGE = 'TODO_ITEM_CHANGE';

export const UPDATE_LIST = 'UPDATE_LIST';

export const UPDATE_ITEM = 'UPDATE_ITEM';

export const EDIT_ITEM = 'EDIT_ITEM';

export const CANCEL = 'CANCEL';