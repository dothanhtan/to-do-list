import React, { useState } from 'react';
import { connect } from 'react-redux';
import { action } from './redux/action';
import { TODO_ITEM_CHANGE, UPDATE_LIST, UPDATE_ITEM, EDIT_ITEM, CANCEL } from './redux/type';
import {Navbar, NavbarBrand, NavbarToggler, Collapse, Nav, NavItem, NavLink, UncontrolledPopover, PopoverHeader, Container, Row, Col, Label, Input, FormGroup, Form, Button } from 'reactstrap';
import List from './List';

const TaskList = ({ todo, dispatch, match }) => {
    const { todoList, todoItem, isAdd, indexUpdate } = todo;
    const { name, level, date } = todoItem;
    const levelOptions = [1, 2, 3, 4, 5];

    const _handleAddTask = (event) => {
        event.preventDefault();
        //const {name, date} = this.state.toDoItem;
    
        if (name === "" || date === "") {
            if (name === "")
                alert("Please enter the name!");
            else 
                alert("Deadline is invalid!");
        }
        else {
            dispatch(action(UPDATE_LIST, [...todoList, todoItem]));
        }
            
        /*if(taskName === '' || taskLevel === '' || taskDate === ''){
            if(taskName === '')
                alert("Please enter the name");
            else
                alert("Deadline is invalid");
        }else{
            this.setState({
                toDoList: this.state.toDoList.concat(toDoItem),
            });
            localStorage.setItem('list', JSON.stringify(toDoItem));
        }*/
        
    }

    const _isChangedName = (e) => {
        //console.log( {...this.state.toDoItem, name: e.target.value})
        /*this.setState({
            toDoItem: {...this.state.toDoItem, name: e.target.value}
        })*/
        dispatch(action(TODO_ITEM_CHANGE, { ...todoItem, name: e.target.value }))
    }
    const _isChangeLevel = (e) => {
        //console.log( {...this.state.toDoItem, level: e.target.value})
        /*this.setState({
            toDoItem: {...this.state.toDoItem, level: e.target.value}
        })*/
        dispatch(action(TODO_ITEM_CHANGE, { ...todoItem, level: e.target.value }))
    }
    const _isChangeDate = (e) => {
        //console.log( {...this.state.toDoItem, date: e.target.value})
        /*this.setState({
            toDoItem: {...this.state.toDoItem, date: e.target.value}
        })*/
        dispatch(action(TODO_ITEM_CHANGE, { ...todoItem, date: e.target.value }))
    }
    const _handleEdit = (index) => {
        /*this.setState({
            toDoItem: this.state.toDoList.find((item, i) => {
                return i === index;
            }),
            isAdd: false,
            indexUpdate: index
        })*/
        dispatch(action(EDIT_ITEM, {
            isAdd: false,
            indexUpdate: index,
            todoItem: todoList.find((item, i) => i === index)
          }));
    }
    const _handleDelete = (index) => {
        /*this.setState({
            toDoList: this.state.toDoList.filter((item, i) => {
                return i !== index;
            })
        })*/
        dispatch(action(UPDATE_LIST, todoList.filter((item, i) => i !== index)));
    }

    const _handleCancel = () => {
        /*this.setState({
            isAdd: true,
            toDoItem: {name: '',level: '1',date: ''}
        })*/
        dispatch(action(CANCEL));
    }

    const _handleUpdate = () => {
        /*this.setState({
            toDoList: this.state.toDoList.map((item, index) =>
              index === this.state.indexUpdate ? this.state.toDoItem : item
            ),
            isAdd: true
          });*/
        dispatch(action(UPDATE_ITEM, todoList.map((item, index) => index === indexUpdate ? todoItem : item )));
    }
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);
    
    return (
        <Container fluid>
            <Navbar color="info" dark expand="lg">
                <NavbarBrand href="/app">React</NavbarBrand>
                <NavbarToggler onClick={toggle} />
                <Collapse isOpen={isOpen} navbar>
                    <Nav className="mr-auto" navbar>
                        <NavItem>
                            <NavLink className="font-weight-bold h5" href="/app">Home</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink className="font-weight-bold h5" href="/tasklist">Product</NavLink>
                        </NavItem>
                    </Nav>
                    <Nav>
                        <Button id="UncontrolledPopover" type="button" color="warning" bsSize="lg" className="font-weight-bold">
                        {match.params.username}
                        </Button>
                        <UncontrolledPopover placement="bottom" target="UncontrolledPopover">
                            <PopoverHeader><a className="text-decoration-none h5" href="/">Logout</a></PopoverHeader>
                        </UncontrolledPopover>
                    </Nav>
                </Collapse>
            </Navbar>
            <Container className="mt-5">
                <h2 className="text-center font-weight-bold text-info mt-4">TO DO LIST</h2>
                <Form>
                    <Row sm="9" md="7">
                        <Col sm="4">
                            <FormGroup>
                                <Label for="name">Name</Label>
                                <Input type="text" value={name} className="form-control" id="name" bsSize="lg" onChange={_isChangedName} placeholder="Enter name of task" />
                            </FormGroup>     
                        </Col>
                        <Col sm="4">
                            <FormGroup>
                                <Label for="level">Level</Label>
                                <Input type="select" value={level} id="level" bsSize="lg" onChange={_isChangeLevel}>
                                {levelOptions.map((item, index) =>
                                    <option key={index} value={item}>{item}</option>
                                )}
                                </Input>
                            </FormGroup>     
                        </Col>
                        <Col sm="4">
                            <FormGroup>
                                <Label for="date">Deadline</Label>
                                <Input type="date" value={date} id="date" bsSize="lg" onChange={_isChangeDate} />
                            </FormGroup>     
                        </Col>
                    </Row>
                    {!isAdd ? (
                    <Row className="mb-5">
                        <Button type="submit" color="primary" onClick={_handleUpdate}><i className="far fa-save"></i> SAVE</Button>&nbsp;
                        <Button type="submit" color="secondary" onClick={_handleCancel}><i className="far fa-times-circle"></i> CANCEL</Button>
                    </Row>   
                    ) : (
                        <Button type="submit" className="mb-5" color="info" onClick={_handleAddTask}><i className="fas fa-plus-circle"></i> ADD</Button>
                    )}
                </Form>    
            </Container>

            <List onEdit={_handleEdit} onDelete={_handleDelete} list={todoList} />
        </Container>
            
    );
}
export default connect(state => ({ todo: state.todo }))(TaskList);
//export default TaskList;