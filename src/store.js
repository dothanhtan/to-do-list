import { createStore} from 'redux';
//import { createStore, applyMiddleware } from 'redux';
//import { createLogger } from 'redux-logger';

import rootReducer from './redux/reducer';

// const logger = createLogger({
//   duration: true
// });

const store = createStore(rootReducer);

export default store;