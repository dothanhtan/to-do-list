import React from 'react';
import {Container, Table, Button} from 'reactstrap'

const List = ({list, onEdit, onDelete}) =>  {
    
    return (
        
        <Container>
            <Table bordered hover>
                <thead>
                    <tr>
                        <th>Name of task</th>
                        <th>Level of task</th>
                        <th>Deadline</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    {list.map((item, index) => 
                    <tr key={index}>
                        <td>{item.name}</td>
                        <td>{item.level}</td>
                        <td>{item.date}</td>
                        <td>
                            <Button color="warning" className="mr-2" onClick={ () => onEdit(index)}>Edit</Button>
                            <Button color="danger" onClick={ () => onDelete(index)}>Delete</Button>
                        </td>
                    </tr>
                    )}
                </tbody>
            </Table>
        </Container>
    );

}

export default List;