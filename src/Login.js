import React from 'react'
import { Form, FormGroup, Label, Input, Button, Container, Col, Card, Row, CardBody, CardTitle} from 'reactstrap';

import { connect } from 'react-redux';
import { action } from "./redux/action";
import { CHANGE_USER } from './redux/type';

const Login = ({ user, history, changeUser }) => {
  const { username, password } = user;

  const _handleLogin = (event) => {
    event.preventDefault();
    if(username === 'thanhtan' && password === '1234'){
      localStorage.setItem('user', JSON.stringify(user));
      //return history.push(`/todo/${username}`);
      return history.push(`/tasklist/${username}`);
    }
  
    else if(username !== 'thanhtan' || password !== '1234'){
      if(username !== 'thanhtan')
        alert("Your username is unvalid");
      else
        alert("Your password is unvalid");
    }
  }
  
  return(
    <Container>
      <Row className="mt-5">
        <Col sm="9" md="7" lg="5" className="mx-auto">
            <Card>
              <CardBody className="bg-warning">
                <CardTitle className="text-center text-uppercase text-danger font-weight-bold h2">Login</CardTitle>
                <Form onSubmit={_handleLogin}>
                    <FormGroup>
                        <Label for="userName" className="text-info font-weight-bold">Email</Label>
                        <Input type="text" bsSize="lg" value={username} onChange={e => changeUser('username', e.target.value)} placeholder="Enter username" required/>
                        
                    </FormGroup>
                    <FormGroup>
                        <Label for="passWork" className="text-info font-weight-bold">Password</Label>
                        <Input type="password" bsSize="lg" value={password} onChange={e => changeUser('password', e.target.value)} placeholder="Enter password" required/>
                        
                    </FormGroup>
                    <Button color="primary" type="submit" className="mt-5" block>Submit</Button>  
                </Form>
              </CardBody>
            </Card>
        </Col>
      </Row>
      
    </Container>
  );
}
 
const mapStateToProps = state => ({ user: state.user.user })

const mapDispatchToProps = dispatch => ({
  changeUser: (key, value) => dispatch(action(CHANGE_USER, { key, value })),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
